/*
Теоретичні питання
1. Що таке події в JavaScript і для чого вони використовуються?
Подія – це сигнал від браузера, що щось сталося. Всі DOM-вузли подають такі сигнали.
Події дозволяють реагувати на взаємодію користувача і викликати відповідні функції або обробники подій.
2. Які події миші доступні в JavaScript? Наведіть кілька прикладів.
Події миші:
click – відбувається, коли клацнули на елемент лівою кнопкою миші (на пристроях із сенсорними екранами воно
відбувається при торканні).
contextmenu – відбувається, коли клацнули на елемент правою кнопкою миші.
mouseover / mouseout – коли миша наводиться на / залишає елемент.
mousedown / mouseup – коли натиснули / відпустили кнопку миші на елементі.
mousemove – під час руху миші.
3. Що таке подія "contextmenu" і як вона використовується для контекстного меню?
Подія "contextmenu" виникає при натисканні правої кнопки миші на елементі. Ця подія часто використовується для
відображення контекстного меню, яке надає користувачеві додаткові опції або дії для вибраного елемента чи області сторінки.
Приклад
Заборонити показ контекстного меню:
div.addEventListener("contextmenu", (e) => {e.preventDefault()});

Практичні завдання
1. Додати новий абзац по кліку на кнопку:
По кліку на кнопку <button id="btn-click">Click Me</button>, створіть новий елемент <p> з текстом "New Paragraph"
і додайте його до розділу <section id="content">
2. Додати новий елемент форми із атрибутами:
Створіть кнопку з id "btn-input-create", додайте її на сторінку в section перед footer.
По кліку на створену кнопку, створіть новий елемент <input> і додайте до нього власні атрибути, наприклад, type,
placeholder, і name. та додайте його під кнопкою.
*/

//1
document.querySelector('#btn-click').addEventListener("click", function() {
    const newParagraph = document.createElement('p');
    newParagraph.innerText = 'New Paragraph';
    // const contentSection = document.querySelector('#content');
    // contentSection.append(newParagraph); - змінила метод вставки елементів, бо в цьому варіанті New Paragraph
    // та input йшли впереміш після обох кнопок
    const btnClick = document.querySelector('#btn-click');
    btnClick.after(newParagraph);
});
//2
const createButton = document.createElement("button");
createButton.id = "btn-input-create";
createButton.innerText = "Create Input";
createButton.style.backgroundColor = "#007bff";
createButton.style.color = "#fff";
createButton.style.padding = "10px 20px";
createButton.style.margin = "10px 0 10px 0";
createButton.style.border = "none";
createButton.style.cursor = "pointer";

const contentSection = document.querySelector("#content");
contentSection.style.cssText = "display: flex; flex-direction: column; align-items: center;";

contentSection.append(createButton);

createButton.addEventListener("mouseover", function() {
    createButton.style.backgroundColor = "#0056b3";
});

createButton.addEventListener("mouseout", function() {
    createButton.style.backgroundColor = "#007bff";
});

createButton.addEventListener("click", function() {

    const newInput = document.createElement('input');
    newInput.type = 'text';
    newInput.placeholder = 'Enter text';
    newInput.name = 'customInput';

    newInput.style.marginTop = '5px';

    contentSection.append(newInput);
});






