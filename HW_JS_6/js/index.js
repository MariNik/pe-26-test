/* Теоретичні питання
1. Опишіть своїми словами, що таке метод об'єкту
Метод об'єкту - це функція, яка прив'язана до об'єкту як властивість. 
Вона може викликатися через звернення до об'єкта та виконує певні дії 
або операції над даними, які знаходяться в цьому об'єкті. 
2. Який тип даних може мати значення властивості об'єкта?
Значення властивості об'єкта можуть бути будь-яким типом даних та містити методи об'єкта.
3. Об'єкт це посилальний тип даних. Що означає це поняття?
Змінні, які вказують на об'єкт, фактично зберігають не сам об'єкт, а лише посилання на нього. 
Такі змінні та об'єкти передаються за посиланням, що означає, що вони вказують на один і той самий об'єкт у пам'яті. 

Практичні завдання
1. Створіть об'єкт product з властивостями name, price та discount. Додайте метод для виведення повної ціни товару з урахуванням знижки. 
Викличте цей метод та результат виведіть в консоль.
2. Напишіть функцію, яка приймає об'єкт з властивостями name та age, і повертає рядок з привітанням і віком,
наприклад "Привіт, мені 30 років". Попросіть користувача ввести своє ім'я та вік
за допомогою prompt, і викличте функцію з введеними даними. Результат виклику функції виведіть з допомогою alert.
3.Опціональне. Завдання:
Реалізувати повне клонування об'єкта.
*/

// 1
const product = {
  name: "Apple",
  price: 13,     
  discount: 0.05, 
  calcTotalPrice: function() {
    let discountedPrice = this.price * (1 - this.discount);
    return discountedPrice;
  }
};

console.log("Повна ціна товару " + product.name + " з урахуванням знижки: " + product.calcTotalPrice() + "гривень");

//2
let userName = prompt("Введіть ім'я:");
let userAge = prompt("Введіть свій вік:");

const user = {
  name: userName,
  age: userAge
};

const createGreeting = (user) => {
    return "Привіт, мене звати " + user.name + " і мені " + user.age + " років";
};

alert(createGreeting(user));

//3
const getDeepClone = (originObject) => {
  const clonedObject = {};

  for (let key in originObject) {
    if (originObject[key] !== null && typeof originObject[key] === "object") {
      clonedObject[key] = getDeepClone(originObject[key]);
    } else {
      clonedObject[key] = originObject[key];
    }
  }

  return clonedObject;
}

const originalObject = {
  brand: 'Toyota',
  model: 'Camry',
  year: 2024,
  isElectric: false,
  features: ['power windows', 'air conditioning', 'cruise control'],
  owner: {
    name: 'Maria',
    age: 25
  }
};

const clonedObject = getDeepClone(originalObject);

console.log(clonedObject);




