/* Теоретичні питання
1. Як можна створити функцію та як ми можемо її викликати?
    Function Declaration:
Функції, оголошені за допомогою ключового слова function, є Function Declaration.
Вони можуть бути викликані до того, як вони оголошені.
    Function Expression:
Функції, які присвоюються змінній або використовуються в якості значення в інших виразах,
називаються Function Expression. Вони оголошуються через let або const.
    Arrow Function:
Стрілкові функції надають коротший синтаксис для створення функцій та автоматичний зв'язок з контекстом виклику.
Вони зазвичай використовуються для анонімних функцій або для коротких функцій. Якщо стрілкова функція приймає
лише один параметр і має один вираз у тілі, то можна опустити круглі дужки та ключове слово return.
    Щоб викликати функцію, використовують ім'я функції, додаючи круглі дужки та передаючи необхідні аргументи,
якщо це потрібно.
2. Що таке оператор return в JavaScript? Як його використовувати в функціях?
    Оператор return використовується для повернення значення з функції.
Він завершує виконання функції та передає значення, яке вказане після return.
function add(x, y) {
    return x + y;
}
3. Що таке параметри та аргументи в функіях та в яких випадках вони використовуються?
    Параметри: Це змінні, що вказуються в оголошенні функції. Вони використовуються для приймання значень,
коли функція викликається.
    Аргументи: Це конкретні значення, які передаються функції при її виклику.
4. Як передати функцію аргументом в іншу функцію?
    Це використовується, наприклад, для здійснення зворотного виклику (callback) або для передачі функції в методи
вищих порядків, такі як map, filter чи reduce.
let result1 = operateOnNumbers(5, 3, add);

Практичні завдання
1. Напишіть функцію, яка повертає частку двох чисел. Виведіть результат роботи функції в консоль.
2. Завдання: Реалізувати функцію, яка виконуватиме математичні операції з введеними користувачем числами.
Реалізувати функцію підрахунку факторіалу числа.
*/
// 1
function divideNumbers(a, b) {
    if (b === 0) {
        console.log("Помилка! Ділення на нуль!");
        return;
    }

    return a / b;
}

console.log("Відповідь: ", divideNumbers(12, 3));

//2
function getNumberFromUser(message) {
    let userInput;
    do {
        userInput = prompt(message);
    } while (isNaN(userInput));
    return +userInput;
}

function getOperation() {
    let operation;
    do {
        operation = prompt("Сюди може бути введено +, -, *, /");
        if (!['+', '-', '*', '/'].includes(operation)) {
            alert('Такої операції не існує');
        }
    } while (!['+', '-', '*', '/'].includes(operation));
    return operation;
}

function calculate(a, b, operation) {
    switch (operation) {
        case '+':
            return a + b;
        case '-':
            return a - b;
        case '*':
            return a * b;
        case '/':
            if (b !== 0) {
                return a / b;
            } else {
                alert("Помилка! Ділення на нуль!");
            }
            break;
    }
}

const numberFirst = getNumberFromUser("Введіть перше число:");
const numberSecond = getNumberFromUser("Введіть друге число:");
const operation = getOperation();

const result = calculate(numberFirst, numberSecond, operation);
console.log(`Відповідь: ${numberFirst} ${operation} ${numberSecond} = ${result}`);

//3
const factorial = (number) => {

    if (number < 0) {
        return "Факторіал визначений тільки для не від'ємних цілих чисел.";
    } else if (number === 0 || number === 1){
        return 1;
    } else {
        return number * factorial(number - 1);
    }
}

let userInput = prompt("Введіть число:");
let userNumber = +userInput;
console.log(factorial(userNumber));

// const numberF = getNumberFromUser("Введіть число:");
// console.log(factorial(numberF)); //використання функції з завдання 2 для валідації




