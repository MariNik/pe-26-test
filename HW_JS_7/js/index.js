/* Теоретичні питання
1. Як можна створити рядок у JavaScript?
Задати значення змінній за допомогою лапок, одинарних або подвійних, використати конструктор рядка.
2. Яка різниця між одинарними (''), подвійними ("") та зворотніми (``) лапками в JavaScript?
Одинарні та подвійні лапки не відрізняються за властивостями. 
Зворотні лапки дозволяють використовувати вбудовані вирази та шаблонні рядки.
3. Як перевірити, чи два рядки рівні між собою?
Для перевірки рівності двох рядків використовують оператор ===
4. Що повертає Date.now()?
Date.now() повертає поточний час в мілісекундах з 1 січня 1970 року (Epoch time).
5. Чим відрізняється Date.now() від new Date()?
Date.now() повертає кількість мілісекунд з Epoch time, але не створює об'єкт Date.
new Date() створює новий об'єкт Date, який представляє поточну дату та час. 

Практичні завдання
1. Перевірити, чи є рядок паліндромом. Створіть функцію isPalindrome, яка приймає рядок str і повертає true, якщо рядок є паліндромом
(читається однаково зліва направо і справа наліво), або false в іншому випадку.
2. Створіть функцію, яка перевіряє довжину рядка. Вона приймає рядок, який потрібно перевірити, максимальну довжину і повертає true, 
якщо рядок менше або дорівнює вказаній довжині, і false, якщо рядок довший. Ця функція стане в нагоді для валідації форми. 
Приклади використання функції:
// Рядок коротше 20 символів
funcName('checked string', 20); // true
// Довжина рядка дорівнює 18 символів
funcName('checked string', 10); // false
3. Створіть функцію, яка визначає скільки повних років користувачу. Отримайте дату народження користувача через prompt. 
Функція повина повертати значення повних років на дату виклику функцію.
*/

//1
function isPalindrome(str) {
    let cleanedStr = '';
    let reversedStr = '';

    for (const ch of str) {
        if (!' ?!,.'.includes(ch)) {
            cleanedStr += ch;
            reversedStr = ch + reversedStr;
        }
    }
    console.log(`Original: "${str}"`);
    console.log(`Cleaned and LowerCase: ${cleanedStr.toLowerCase()} \nReversed and LowerCase: ${reversedStr.toLowerCase()}`);

    return cleanedStr.toLowerCase() === reversedStr.toLowerCase();
}

console.log(isPalindrome('Палиндром-палиндром'));
console.log(isPalindrome('А роза упала на лапу Азора?'));

//2
function checkStringLength(str, maxLength) {
    return str.length <= maxLength; 
}
console.log(checkStringLength('checked string', 20));
console.log(checkStringLength('checked string', 10));

//3
function calculateAge(birthDateInput) {
    const currentDate = new Date();
    const birthDate = new Date(birthDateInput);

    if (isNaN(birthDate)) {
        console.log("Некоректна дата народження.");
        return;
    }
    
    let ageInMilliseconds = currentDate - birthDate;
    let ageInYears = ageInMilliseconds / (1000 * 60 * 60 * 24 * 365.2425);
    
    const age = Math.floor(ageInYears);
    return age;
}

const userBirthDate = prompt('Введіть вашу дату народження (рррр-мм-дд):');
const userAge = calculateAge(userBirthDate);
console.log(`Ваш вік: ${userAge} років.`);



