/* Теоретичні питання
1. В чому відмінність між setInterval та setTimeout?
 * setTimeout дозволяє нам запускати функцію один раз через певний інтервал часу.
 * setInterval дозволяє нам запускати функцію багаторазово, починаючи через певний інтервал часу, а потім постійно
повторюючи у цьому інтервалі.
2. Як припинити виконання функції, яка була запланована для виклику з використанням setTimeout та setInterval?
 * Для припинення виконання функції, запланованої за допомогою setTimeout, можна використовувати clearTimeout.
 * Для setInterval використовується clearInterval. Обидва методи приймають ідентифікатор таймера, який повертається
функцією setTimeout або setInterval.

Практичне завдання:
-Створіть HTML-файл із кнопкою та елементом div.
-При натисканні кнопки використовуйте setTimeout, щоб змінити текстовий вміст елемента div через затримку 3 секунди.
Новий текст повинен вказувати, що операція виконана успішно.
Практичне завдання 2:
Реалізуйте таймер зворотного відліку, використовуючи setInterval. При завантаженні сторінки виведіть зворотний відлік
від 10 до 1 в елементі div. Після досягнення 1 змініть текст на "Зворотній відлік завершено".
*/

let operationDiv = document.querySelector('.text-container');

document.querySelector('.button').addEventListener('click', () => {
    setTimeout(() => {
        operationDiv.innerText = 'The operation was completed successfully';
    }, 3000);
});



let count = 9;
let timerDiv = document.querySelector('.timer');

const timerInterval = setInterval(() => {
    timerDiv.innerText = `${count} seconds left until ...`;
    if (count === 0) {
        clearInterval(timerInterval);
        timerDiv.innerHTML = '<strong>nothing.</strong> The countdown is over.';
    }
    count--;
}, 1000);
