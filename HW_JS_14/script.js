/* Теоретичні питання
1. В чому полягає відмінність localStorage і sessionStorage?
 * localStorage: Зберігає дані на довший термін (призначений для тривалого зберігання).
 Дані залишаються в localStorage навіть після закриття браузера і перезапуску комп'ютера.
 * sessionStorage: Зберігає дані тільки на час життя поточного сеансу браузера.
 Якщо закрити вікно або вкладку браузера, дані в sessionStorage будуть видалені.
2. Які аспекти безпеки слід враховувати при збереженні чутливої інформації, такої як паролі, за допомогою localStorage
чи sessionStorage?
Вміст localStorage та sessionStorage доступний для читання і запису через JavaScript, і це може бути легко використано
для отримання чутливих даних. Одна з основних причин полягає в тому, що ці дані знаходяться на клієнтському боці, і
тому localStorage та sessionStorage не є надійним місцем для зберігання чутливих даних.
3. Що відбувається з даними, збереженими в sessionStorage, коли завершується сеанс браузера?
Коли сеанс браузера завершується (користувач закриває вікно або вкладку), дані в sessionStorage автоматично видаляються.
sessionStorage існує лише на поточній вкладці браузера.Інша вкладка з тією ж сторінкою матиме інше сховище.


Практичне завдання:
Реалізувати можливість зміни колірної теми користувача.
Технічні вимоги:
- Взяти готове домашнє завдання HW-4 "Price cards" з блоку Basic HMTL/CSS.
- Додати на макеті кнопку "Змінити тему".
- При натисканні на кнопку - змінювати колірну гаму сайту (кольори кнопок, фону тощо) на ваш розсуд.
При повторному натискання - повертати все як було спочатку - начебто для сторінки доступні дві колірні теми.
- Вибрана тема повинна зберігатися після перезавантаження сторінки.
Примітки:
- при виконанні завдання перебирати та задавати стилі всім елементам за допомогою js буде вважатись помилкою;
- зміна інших стилів сторінки, окрім кольорів буде вважатись помилкою.
Додаткові матеріали: https://developer.mozilla.org/en-US/docs/Web/CSS/Using_CSS_custom_properties. */

let btn = document.getElementById("theme-button");
let link = document.getElementById("theme-link");
let lightTheme = "css/light-mode.css";
let darkTheme = "css/dark-mode.css";

const savedTheme = localStorage.getItem("theme");

if (savedTheme) {
    document.documentElement.className = savedTheme;
    link.setAttribute("href", savedTheme === "light" ? lightTheme : darkTheme);
}

btn.addEventListener("click", changeTheme);

function changeTheme() {

    let currTheme = link.getAttribute("href");
    let theme = "";

    if (currTheme === lightTheme) {
        currTheme = darkTheme;
        theme = "dark";
    } else {
        currTheme = lightTheme;
        theme = "light";
    }

    localStorage.setItem("theme", theme);
    link.setAttribute("href", currTheme);
}
