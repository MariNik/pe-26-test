2.1 Для того, щоб додати нове домашнє завдання в новий репозиторій є 2 шляхи. Перший спосіб: Створюємо новий репозиторій на сайті Gitlab.com, клонуємо його на свій компʼютер за допомогою команди git clone https://gitlab.com/...
Перебуваючи в папці з проектом, пишемо команду git add . 
Пишемо команду git commit -m "commit" 
Далі - git push  
Оновлюємо проект на гітлабі 

2.2 Заходимо в папку, де зберігається наш проект. 
Відкриваємо термінал у цій папці.
Пишемо команду git init 
Пишемо команду git remote add origin https://gitlab.com/...
Перебуваючи в папці з проектом, пишемо команду git add . 
Пишемо команду git commit -m "Initial commit" 
Далі - git push -u origin master. 
Оновлюємо проект на гітлабі 