/* 
Теоретичні питання
1. Опишіть своїми словами що таке Document Object Model (DOM)
    DOM - це програмне представлення HTML-або XML-документа у вигляді деревоподібної структури, 
де кожен елемент документа представлений об'єктом, і може бути змінений за допомогою JavaScript
2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?
    innerHTML: Властивість, яка дозволяє отримати або встановити HTML вміст елементу. Включає HTML теги.
    innerText: Властивість, яка отримує або встановлює текстовий вміст елементу, при цьому ігноруючи HTML теги.
3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
    За допомогою getElementById для отримання елемента за його ідентифікатором.
    За допомогою querySelector для отримання першого елемента, який відповідає CSS селектору.
    За допомогою querySelectorAll для отримання всіх елементів, які відповідають CSS селектору.
Вибір методу залежить від конкретного використання, але querySelector та querySelectorAll гнучкіше.
4. Яка різниця між nodeList та HTMLCollection?
    NodeList та HTMLCollection є колекціями вузлів.Обидві структури досить схожі, але різниця полягає в тому, як вони оновлюються та які методи їх повертають. 
HTMLCollection дозволяє звертатися до елементів не тільки за індексом, але й за назвою за допомогою методу namedItem; 
NodeList може бути не лише «живою» колекцією, а й статичною. Така колекція не оновлюється з появою на сторінці нових елементів

Практичні завдання
 1. Знайдіть всі елементи з класом "feature", запишіть в змінну, вивести в консоль.
 Використайте 2 способи для пошуку елементів. 
 Задайте кожному елементу з класом "feature" вирівнювання тексту по - центру(text-align: center).
 
 2. Змініть текст усіх елементів h2 на "Awesome feature".

 3. Знайдіть всі елементи з класом "feature-title" та додайте в кінець тексту елементу знак оклику "!".
 */

//1
const featureList = document.getElementsByClassName("feature");
console.log(featureList);

const secondFeatureList = document.querySelectorAll(".feature");
console.log(secondFeatureList);

secondFeatureList.forEach(element => {
    element.style.textAlign = "right"; //для демонстрації, так як у css вже вказано text-align: center;
});

//2
const h2Elements = document.querySelectorAll("h2");

h2Elements.forEach(element => {
    element.textContent = "Awesome feature";
});

//3
const featureTitleElements = document.querySelectorAll(".feature-title");

featureTitleElements.forEach(element => {
    element.textContent += "!";
});
