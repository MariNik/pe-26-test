/*
Теоретичні питання
1. Які способи JavaScript можна використовувати для створення та додавання нових DOM-елементів?
    document.createElement(tagName): Створює новий DOM-елемент з вказаним тегом.
    document.createTextNode(text): Створює текстовий вузол з вказаним текстом.
    element.appendChild(childElement): Додає дочірній елемент в кінець батьківського елемента.
    element.insertBefore(newElement, referenceElement): Вставляє новий елемент перед вказаним елементом.
2. Опишіть покроково процес видалення одного елементу (умовно клас "navigation") зі сторінки.
const elementToRemove = document.querySelector('.navigation');
elementToRemove.remove();
3. Які є методи для вставки DOM-елементів перед/після іншого DOM-елемента?
append/prepend/before/after
element.insertAdjacentElement(position, newElement)
position може бути 'beforebegin', 'afterbegin', 'beforeend' або 'afterend'.

Практичні завдання
 1. Створіть новий елемент <a>, задайте йому текст "Learn More" і атрибут href з посиланням на "#". Додайте цей елемент в footer після параграфу.
 2. Створіть новий елемент <select>. Задайте йому ідентифікатор "rating", і додайте його в тег main перед секцією "Features".
 Створіть новий елемент <option> зі значенням "4" і текстом "4 Stars", і додайте його до списку вибору рейтингу.
 Створіть новий елемент <option> зі значенням "3" і текстом "3 Stars", і додайте його до списку вибору рейтингу.
 Створіть новий елемент <option> зі значенням "2" і текстом "2 Stars", і додайте його до списку вибору рейтингу.
 Створіть новий елемент <option> зі значенням "1" і текстом "1 Star", і додайте його до списку вибору рейтингу.
*/

//1
const linkElem = document.createElement('a');
linkElem.innerText = 'Learn More';
linkElem.href = '#';
const footer = document.querySelector('footer');
footer.append(linkElem);

//2
const selectElem = document.createElement('select');
selectElem.id = 'rating';
const main = document.querySelector('main');
main.prepend(selectElem);

let options = [4, 3, 2, 1];
options.forEach((element)=> {
    let option = document.createElement('option');
    option.value = element.toString();
    option.text = element + (element > 1 ? ' Stars' : ' Star');
    selectElem.appendChild(option);
});
