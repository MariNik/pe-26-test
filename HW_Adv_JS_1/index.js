
/* Теоретичне питання
Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
    Це механізм, за допомогою якого об'єкти можуть успадковувати властивості та методи інших об'єктів.
    Кожен об'єкт має прототип, який вказує на інший об'єкт, від якого він успадковує властивості та методи.
    Коли властивість або метод не знаходиться в об'єкті, JavaScript шукає його в прототипі.
Для чого потрібно викликати super() у конструкторі класу-нащадка?
    Виклик super() у конструкторі класу-нащадка використовується для виклику конструктора батьківського класу.
    Це дозволяє нащадку успадкувати властивості та методи батьківського класу та виконувати додаткові дії.
    Наприклад, якщо у батьківському класі є конструктор, який встановлює певні властивості, то виклик super() у
    конструкторі класу-нащадка дозволяє виконати цей конструктор батьківського класу та встановити властивості для
    нащадка.


Завдання
Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата).
Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.
Створіть гетери та сеттери для цих властивостей.
Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).
Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.
Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.
*/
class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name;
    }

    set name(value) {
        this._name = value;
    }

    get age() {
        return this._age;
    }

    set age(value) {
        this._age = value;
    }

    get salary() {
        return this._salary;
    }

    set salary(value) {
        this._salary = value;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
    }

    get lang() {
        return this._lang;
    }

    set lang(value) {
        this._lang = value;
    }

    get salary() {
        return super.salary * 3;
    }
}

let programmer1 = new Programmer('Ivan', 99, 10000, 'JavaScript');
let programmer2 = new Programmer('Oleg', 15, 90000, 'Java');

console.log(programmer1);
console.log(programmer2);

console.log(programmer1.name);
console.log(programmer1.age);
console.log(programmer1.salary);
console.log(programmer1.lang);
